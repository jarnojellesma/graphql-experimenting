import gql from 'graphql-tag'
import { makeExecutableSchema } from'graphql-tools';
import data from './data';
import pubsub from './pubsub';

const typeDefs = gql`

  type Query {
    comments: [Comment]
  }
  type Mutation {
    addComment(text: String): Comment
  }

  type Subscription {
    commentAdded: Comment
  }

  type Comment {
    id: String
    text: String
  }

  schema {
    query: Query
    mutation: Mutation
    subscription: Subscription
  }
`

const resolvers = {
  Query: { 
    comments: () => {
      console.log('comments resolver')
      return data.comments 
    }
  },
  Mutation: { 
    addComment: () => {
      console.log('add comment mutation')
      pubsub.publish('commentAdded', { commentAdded: data.comment })
      console.log('published')
      return data.comment 
    }
  },
  Subscription: {
    commentAdded: {
      subscribe: () => {
        console.log('subscribe')
        return pubsub.asyncIterator('commentAdded')
      },
      /*
      resolve: (payload) => {
        return {
          customData: payload,
        };
      }, */
    }
  }
}
const schema = makeExecutableSchema({
  typeDefs,
  resolvers
});

export default schema
