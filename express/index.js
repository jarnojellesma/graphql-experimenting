import { PubSub } from 'graphql-subscriptions';
import { createServer } from 'http';
import { SubscriptionServer } from 'subscriptions-transport-ws';
import { execute, subscribe } from 'graphql';
import schema from './my-schema';
import express from 'express';
import bodyParser from 'body-parser';
import { graphqlExpress, graphiqlExpress } from 'apollo-server-express';
import cors from 'cors';


const pubsub = new PubSub();

const app = express();
const HTTP_PORT = 3030

app.use(cors()); // Needs to be secured origin we

app.use('/graphql', bodyParser.json(), graphqlExpress({ schema }));
app.use('/graphiql', graphiqlExpress({
  endpointURL: '/graphql',
  subscriptionsEndpoint: `ws://localhost:3030/subscriptions`
}));

const server = createServer(app);

server.listen(HTTP_PORT, () => {
  console.log(`Apollo Server is now running on http://localhost:${HTTP_PORT}`);

  new SubscriptionServer({
    execute,
    subscribe,
    schema
  }, {
    server,
    path: '/subscriptions',
  });
  console.log('websocket server created');
}); 
