'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _templateObject = _taggedTemplateLiteral(['\n\n  type Mutation {\n    addComment(text: String!): Comment\n  }\n\n  type Subscription {\n    newComment: Comment\n  }\n\n  type Comment {\n    id: String\n    text: String\n  }\n\n  schema {\n    mutation: Mutation\n    subscription: Subscription\n  }\n'], ['\n\n  type Mutation {\n    addComment(text: String!): Comment\n  }\n\n  type Subscription {\n    newComment: Comment\n  }\n\n  type Comment {\n    id: String\n    text: String\n  }\n\n  schema {\n    mutation: Mutation\n    subscription: Subscription\n  }\n']);

var _graphqlTag = require('graphql-tag');

var _graphqlTag2 = _interopRequireDefault(_graphqlTag);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _taggedTemplateLiteral(strings, raw) { return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var schema = (0, _graphqlTag2.default)(_templateObject);

exports.default = schema;