'use strict';

var _graphqlSubscriptions = require('graphql-subscriptions');

var _http = require('http');

var _subscriptionsTransportWs = require('subscriptions-transport-ws');

var _graphql = require('graphql');

var _mySchema = require('./my-schema');

var _mySchema2 = _interopRequireDefault(_mySchema);

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _bodyParser = require('body-parser');

var _bodyParser2 = _interopRequireDefault(_bodyParser);

var _apolloServerExpress = require('apollo-server-express');

var _cors = require('cors');

var _cors2 = _interopRequireDefault(_cors);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var pubsub = new _graphqlSubscriptions.PubSub();

var app = (0, _express2.default)();
var HTTP_PORT = 3030;

app.use((0, _cors2.default)());
app.use('/graphql', function (req, res, next) {
  console.log('graphql in');
  next();
});
app.use('/graphql', _bodyParser2.default.json(), (0, _apolloServerExpress.graphqlExpress)({ schema: _mySchema2.default }));
app.listen(HTTP_PORT, function () {
  console.log('Server is now running on http://localhost:' + HTTP_PORT);
});

/*

const WS_PORT = 5000;

// Create WebSocket listener server
const websocketServer = createServer((request, response) => {
  response.writeHead(404);
  response.end();
});

// Bind it to port and start listening
websocketServer.listen(WS_PORT, () => console.log(
  `Websocket Server is now running on http://localhost:${WS_PORT}`
));

const subscriptionServer = SubscriptionServer.create(
  {
    schema,
    execute,
    subscribe,
  },
  {
    server: websocketServer,
    path: '/graphql',
  },
);
*/