const path = require('path');
const webpack = require('webpack');

module.exports = {
  modules: [
    //'@nuxtjs/apollo',
    'bootstrap-vue/nuxt',
  ],
  /*apollo: {
    clientConfigs: {
      default: '~/apollo/client-configs/default.js'
    }
  }, */
  router: {
    middleware: 'check-auth',
  },
  build: {
    extend(config) {
      config.module.rules.push({
        test: /\.(graphql|gql)$/,
        exclude: /node_modules/,
        loader: 'graphql-tag/loader',
      });
      config.module.rules.push({
        test: /\.scss?$/,
        loaders: ['css-loader', 'sass-loader']
      })
      config.node = {
        fs: 'empty',
        tls: 'empty'
      }
    }
  }, 
};
