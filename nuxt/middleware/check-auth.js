import feathersClient from '~/plugins/feathers';

const cookieName = 'feathers-jwt';

export let accessToken = '';

export function getTokenFromRequest (req) {
  if (!req || !req.headers || !req.headers.cookie) return;
  const jwtCookie = req.headers.cookie.split(';').find(c => c.trim().startsWith(`${cookieName}=`));

  if (!jwtCookie) return;
  return jwtCookie.split('=')[1];
}

export default function (context) {
  const {isServer, store, req} = context;

  accessToken = (isServer) ? getTokenFromRequest(req) : window.localStorage.getItem(cookieName);

  if (!accessToken) return;

  return store.dispatch('auth/jwt', accessToken)
    .catch(() => {
      // Ignore invalid JWT
    }); 
}
