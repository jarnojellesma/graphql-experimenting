import Vuex from 'vuex'
import auth from './auth';

const store = {
  modules: {
    auth,
  },
};

const createStore = () => {
  return new Vuex.Store(store)
}

export default createStore;
