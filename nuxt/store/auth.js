import feathersClient from '~/plugins/feathers';
import apolloClient from '~/plugins/apollo';

const state = () => ({
  user: null,
});

const getters = {
  isAuthenticated: state => {
    return !!state.user;
  },
};

const mutations = {
  SET_USER(state, user) {
    state.user = user || null;
  },
};

const actions = {
  async jwt(store, accessToken) {
    const response = await feathersClient.authenticate({ strategy: 'jwt', accessToken })
    
    const payload = await feathersClient.passport.verifyJWT(response.accessToken);
    
    const user = await feathersClient.service('users').get(payload.userId);

    store.commit('SET_USER', user);
  },
  async login(store, { email, password }) {
    const response = await feathersClient.authenticate({ strategy: 'local', email, password })
  
    const payload = await feathersClient.passport.verifyJWT(response.accessToken);

    const user = await feathersClient.service('users').get(payload.userId);

    store.commit('SET_USER', user);
  },
  async logout(store) {
    try {
      await feathersClient.logout()
      apolloClient.resetStore()
      store.commit('SET_USER', null);
    } catch (e) {
      throw new Error(e);
    }
  },

  async register(store, user) {
    try {
      const result = await feathersClient.service('users').create(user)
    } catch (e) {
      console.log(e);
    }
    await store.dispatch('login', { email: user.email, password: user.password });
  },
};

export default {
  namespaced: true,  
  state,
  getters,
  mutations,
  actions,
}