import 'isomorphic-fetch';
import Vue from 'vue'
import VueApollo from 'vue-apollo'
import { ApolloClient } from 'apollo-client'
import { split } from 'apollo-link';
import { HttpLink } from 'apollo-link-http'
import { WebSocketLink } from 'apollo-link-ws';
import { InMemoryCache } from 'apollo-cache-inmemory'
import { getMainDefinition } from 'apollo-utilities'
import ws from 'ws';
import { setContext } from 'apollo-link-context';
import { SubscriptionClient } from 'subscriptions-transport-ws';

/* Http */
const httpLink = new HttpLink({
  uri: 'http://localhost:3030/graphql',
});

const httpAuthLink = setContext(() => ({
  headers: { 
    authorization: localStorage.getItem('feathers-jwt') || null,
  }
}));

const httpAuthedLink = httpAuthLink.concat(httpLink);

/* Websocket */
let wsAndHttpLink;

if (!process.server) {
  const wsClient = new SubscriptionClient(`ws://localhost:3030/subscriptions`, {
    reconnect: true,
    timeout: 30000,
    connectionParams: () => {
      return { 
        Authorization: `test token` 
      }
    }
  });

  const wsLink = new WebSocketLink(wsClient);

  wsAndHttpLink = split(
    ({ query }) => {
      const { kind, operation } = getMainDefinition(query);
      return kind === 'OperationDefinition' && operation === 'subscription';
    },
    wsLink,
    httpAuthedLink,
  );
}


console.log(process.server ? 'httpLink' : 'http and ws link')

const apolloClient = new ApolloClient({
  link: process.server ? httpAuthedLink : wsAndHttpLink,
  cache: new InMemoryCache(),
}); 

export default apolloClient;
