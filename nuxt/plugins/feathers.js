const feathers = require('@feathersjs/client');
const rest = require('@feathersjs/rest-client');
const authentication = require('@feathersjs/authentication-client');
const storage = require('../helpers/ssr-storage');
const axios = require('axios');

const host = 'localhost';
const port = '3030';

const restClient = rest(`http://${host}:${port}`);
const client = feathers()
  .configure(restClient.axios(axios))
  .configure(authentication({storage}));
  
module.exports = client;
