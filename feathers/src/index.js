/* eslint-disable no-console */
import 'module-alias/register';
import logger from 'winston';
import app from './app';
const port = app.get('port');
import { SubscriptionServer } from 'subscriptions-transport-ws';
import { execute, subscribe } from 'graphql';

import schema from '@graphql/executableSchema';

process.on('unhandledRejection', (reason, p) =>
  logger.error('Unhandled Rejection at: Promise ', p, reason)
);

const server = app.listen(port);

server.on('listening', () => {
  logger.info('Feathers application started on http://%s:%d', app.get('host'), port)

  new SubscriptionServer({
    execute,
    subscribe,
    schema,
    onConnect: (connectionParams, webSocket) => {
      console.log('subscriptionServer onConnect');
      console.log(connectionParams);
   }
  }, {
    server,
    path: '/subscriptions'
  });
});
