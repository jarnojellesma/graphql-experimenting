const users = require('./users/users.service.js');
const graphql = require('./graphql/graphql.service.js');
const todos = require('./todos/todos.service.js');
module.exports = function () {
  const app = this; // eslint-disable-line no-unused-vars
  app.configure(users);
  app.configure(todos);

  app.configure(graphql);
};
