const bodyParser = require('body-parser');
const graphqlExpress = require('graphql-server-express').graphqlExpress;
const graphiqlExpress = require('graphql-server-express').graphiqlExpress;
const makeExecutableSchema = require('graphql-tools').makeExecutableSchema;
const apolloUploadExpress = require('apollo-upload-server').apolloUploadExpress;

import schema from '@graphql/executableSchema';

const createContext = (app) => {
  return {
    
  }
}

module.exports = function () {
  const app = this;

  app.use('/graphql', bodyParser.json(), (req, res, next) => {
    console.log('graphql in');
    next();
  });
  
  app.use('/graphql', graphqlExpress(async (req) => {
    const token = req.feathers.headers.authorization;
    const params = req.feathers;
    const secret = app.get('authentication').secret;
    params.provider = 'graphql'
    
    console.log('Token: ' + token);

    if (token !== null && token !== undefined && String(token) !== 'null') {
      try {
        const payload = await app.passport.verifyJWT(token, { secret });
        params.user = await app.service('users').get(payload.userId);
        params.authenticated = true;
      } catch (e) {
        console.log('verify accessToken error');
        console.log(e);
      }
    }
    
    return {
      schema,
      context: {
        app,
        params,
      }
    };
  }));

  app.use('/graphiql', graphiqlExpress({
    endpointURL: '/graphql',
    subscriptionsEndpoint: `ws://localhost:3030/subscriptions`
  }));
};
