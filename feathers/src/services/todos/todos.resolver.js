import pubsub from '@server/services/graphql/pubsub';

module.exports = {
  Query: { 
    todos: (root, args, context) => {
      const { app, params } = context;
      return app.service('todos').find(Object.assign(params, { paginate: false }));
    }
  },
  Mutation: { 
    createTodo: async (root, args, context) => {
      const { app, params } = context;
      const todo = app.service('todos').create(args, params);
      pubsub.publish('todoCreated', { todoCreated: todo})
      return todo;
    }
  },
  
  Subscription: {
    todoCreated: {
      subscribe: () => {
        console.log('subscribe')
        return pubsub.asyncIterator('todoCreated')
      },
    }
  }
}
