import gql from 'graphql-tag';

const fs = require('fs');
const path = require('path');
const typeDefs = [fs.readFileSync(path.join(__dirname, "schema.gql"), "utf8")];
const resolvers = require('@server/services/graphql/resolvers');
const { makeExecutableSchema } = require('graphql-tools');

const schema = makeExecutableSchema({
  typeDefs,
  resolvers,
});

export default schema;
